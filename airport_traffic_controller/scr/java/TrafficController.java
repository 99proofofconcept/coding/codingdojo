package Mediator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TrafficController implements AirTrafficControl
{
    private List<AirPlane> airPlanes = new ArrayList<>();

    @Override
    public void registerAirPlane(AirPlane airPlane)
    {
        if (!this.airPlanes.contains(airPlane))
        {
            this.airPlanes.add(airPlane);
        }
    }

    @Override
    public void receiveHeightAirPlane(AirPlane airPlane)
    {
        int heightDifference = 1000;

        for (AirPlane currentAirPlane : airPlanes.stream()
                .filter((item) -> item != airPlane)
                .collect(Collectors.toList()))
        {
                if (Math.abs(currentAirPlane.getAltitude() - airPlane.getAltitude()) < heightDifference)
            {
                airPlane.climb(heightDifference);
                currentAirPlane.warnOfAirspaceIntrusionBy(airPlane, currentAirPlane);
            }
        }
    }
}
