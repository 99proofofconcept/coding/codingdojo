package ObserverPattern;

import java.util.ArrayList;
import java.util.List;

public class Notification {

    protected List<Screen> screens = new ArrayList<>();

    public void add(Screen screen)
    {
        screens.add(screen);
    }

    public void remove(Screen screen)
    {
        screens.remove(screen);
    }

    protected void broadcast()
    {
        for(Screen screen : screens)
        {
            screen.update();
        }
    }
}
